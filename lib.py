import numpy as np
from fipy import CellVariable, Grid1D
from IPython import embed

# initially empty structure where we will store temperature and density profiles 
class Profiles:
	pass

# structure where we store geometry information
class Geometry:
	# initialize geometry structure using input parameters 
    def __init__(self, inpars):  
        #TODO: define here the mesh, etc.


# structure for storing physical constants
class Constants: 
	pi = np.pi
	# more to come...

# routine for initializing temperature profiles
def init_profiles(inpars, geo, consts):
	profiles = Profiles() # instantiate profiles structure

	# TODO!! Set up profiles.T as a FiPy CellVariable with your desired initial conditions, and correct boundary conditions

	return profiles