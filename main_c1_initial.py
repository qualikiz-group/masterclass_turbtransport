from fipy import Variable, FaceVariable, CellVariable, ExplicitDiffusionTerm, DiffusionTerm, TransientTerm, Viewer
from fipy.tools import numerix
import numpy as np
import matplotlib.pyplot as plt
import matplotlib as mpl
from IPython import embed

from lib_c1_initial import *   
from parameters_c1_initial import *

# Instantiate input, constant, and geometry structures
inpars = Input()  # all inputs from parameters.py are now in inpars structure
consts = Constants() # structure for physical constants
geo = Geometry(inpars) #  structure for all the geometric quantities (e.g. the mesh). ## TODO!! Define mesh in Geometry class

# Initialize profiles
profiles = init_profiles(inpars, geo) # initialize T. Note that profiles.T is a fipy CellVariable

# instantiate fipy viewer object for plotting
viewer = Viewer(vars=profiles.T, datamin=0., datamax=inpars.viewer_datamax)

# run simulation! 
## TODO!!

